package com.intellisrc.groovy

import groovy.transform.CompileStatic

@CompileStatic
class App {
    static void main(String[] args) {
    }
}
